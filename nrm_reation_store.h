
//#include "AssocVector.h"
//#include "ttimer.h"
#include <array>
#include <cmath>
#include <list>
#include <map>
#include <random>
#include <stack>
#include <unordered_map>
#include <vector>

#pragma once

/* Differnt kind of boost.heaps can be selected if needed. In my experince, fibonacci heap performs
 * best. The used_boost_heap is defined as a proxy for whatever boost heap is used.*/

/*
#include "boost/heap/pairing_heap.hpp"
 template <typename T> using used_boost_heap = boost::heap::pairing_heap<T>;
*/

#include "boost/heap/fibonacci_heap.hpp"
template <typename T> using used_boost_heap = boost::heap::fibonacci_heap<T>;

/*
#include "boost/heap/binomial_heap.hpp"
 template <typename T> using used_boost_heap = boost::heap::binomial_heap<T>;
*/

/*
#include "boost/heap/skew_heap.hpp"
 template <typename T>
 using used_boost_heap = boost::heap::skew_heap<T, boost::heap::mutable_<true>>;
*/

/*
#include "boost/heap/d_ary_heap.hpp"
 template <typename T>
 using used_boost_heap =
    boost::heap::d_ary_heap<T, boost::heap::arity<2>, boost::heap::mutable_<true>>;
*/

template <typename T> struct reaction_store {
public:
  struct que_element;

  /* a refernce object, used to later make changes to a stored reaction */
  struct reaction_handle {
    typename used_boost_heap<que_element>::handle_type itr;
    size_t test_idx;
  };

  /* Returns a string with the configuration of this reaction_store*/
  std::string about() {
    std::string x = "Useing Next reaction method";
    return x;
  }

  /* add a reaction to the container. This specific reaction may later be refered to via the
   * returned reaction_handle */
  reaction_handle add(const T &val) {

    auto old_size = size();

    double delta_time = get_exponetial(val.calculated_rate);

    que_element qe;
    qe.payload = val;
    test_count++;
    qe.idx = test_count;
    qe.time = time + delta_time;
    auto iter = que.push(qe);

    assert_2(old_size + 1 == size());

    (*iter).internal_handle = iter;
    reaction_handle hdl;
    hdl.itr = iter;
    hdl.test_idx = qe.idx;

    assert_3(contains(hdl));

    return hdl;
  }

  /* standart C++ type performance optimization */
  void reserve(size_t){ /* not applicable to this implementation */ };

  /* remove a reaction referd to by hdl */
  void remove(const reaction_handle &hdl) {
    assert_3(contains(hdl));
    assert_3(hdl.itr == (*hdl.itr).internal_handle);

    auto old_size = que.size();

    (*hdl.itr).payload.remove();

    assert_3(contains(hdl));
    que.erase(hdl.itr);
    assert_3(!contains(hdl));

    assert_2(old_size - 1 == que.size());
    assert_3(!contains(hdl));
  }

  /* is the container empty? */
  bool empty() { return que.empty(); }

  /* modification of an element requers updating of the branch in the tree */
  void modify(const reaction_handle &hdl, const double = 0, const double = 0) {
    assert_3(contains(hdl));
    T old_val = *get(hdl); //(*hdl.elm).payload;
    double new_time = time + get_exponetial(old_val.calculated_rate);
    que_element qe;
    qe = *(hdl.itr);
    qe.time = new_time;
    que.update(hdl.itr, qe);

    // nothing needs to be done, as there is no caching
  }

  /* retrive the undelying reaction of a hdl object */
  T *get(const reaction_handle &hdl) {
    assert_3(contains(hdl));
    assert_3(hdl.test_idx == (*hdl.itr).idx) assert_3(hdl.itr == (*hdl.itr).internal_handle);

    return &((*hdl.itr).payload);
  }

  /* Select the next reaction to fire. Returend are the reaction and the timestep delta T */
  std::pair<T const *, double> select() {
    assert_2(!que.empty());

    que_element qe = que.top();
    double delta_time = qe.time - time;
    assert_2(delta_time >= 0);

    time += delta_time;
    qe.time = time + get_exponetial(qe.payload.calculated_rate);

    assert_3(que.ordered_begin()->idx == que.top().idx);

    auto first_hdl = que.top().internal_handle;

    que.update(first_hdl, qe);

    return std::make_pair(&((*first_hdl).payload), delta_time);
  }

  /* don't use other than for testing. This is very slow... */
  double get_sum() {
    double sum = 0;
    for (const auto &val : que) {
      sum += val.payload.calculated_rate;
    }
    return sum;
  }

  /* how many reactions are stored */
  size_t size() const { return que.size(); }

  struct que_element {
    T payload;
    double time;
    size_t idx;
    /* limited functionality workaround */
    mutable typename used_boost_heap<que_element>::handle_type internal_handle;
    bool operator<(const que_element &e) const { return time > e.time; }
  };

private:
  /* only testing */
  bool contains(const reaction_handle &hdl) {
    std::set<size_t> test_set;
    for (auto &x : que) {
      if (test_set.find(x.idx) != test_set.end()) {
        throw std::runtime_error("double contain!");
        return false;
      }
      test_set.insert(x.idx);
    }
    return test_set.end() != test_set.find(hdl.test_idx);
  }

  double time = 0;
  used_boost_heap<que_element> que;

  size_t test_count = 0;

  std::random_device rd;
  std::mt19937_64 mersenneGenerator = std::mt19937_64(rd());

  double get_exponetial(double rate) {

    std::exponential_distribution<double> expoDist(rate);
    return expoDist(mersenneGenerator);
  }
};
