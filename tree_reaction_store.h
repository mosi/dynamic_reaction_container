
//#include "AssocVector.h"
//#include "ttimer.h"
#include <array>
#include <cmath>
#include <list>
#include <map>
#include <random>
#include <stack>
#include <unordered_map>
#include <vector>

#pragma once

template <typename T> struct reaction_store {
public:
  /* Returns a string with the configuration of this reaction_store*/
  std::string about() {
    std::string x = "Useing Direct Method with tree: ";
    if (use_swapping) {
      x += "using swapping ";
    } else {
      x += "no swapping ";
    }
    x += " with a fanout of " + std::to_string(node_size);

    return x;
  }

  /* a refernce object, used to later make changes to a stored reaction */
  struct reaction_handle {
    size_t id;
  };

  /* add a reaction to the container. This specific reaction may later be refered to via the
   * returned reaction_handle */
  reaction_handle add(const T &val) {

    size_t new_id = get_id();
    auto location = get_tree_pos();

    if (lookup.size() <= new_id) {
      /* adding at the end -> need to allocate new spot */
      assert_2(lookup.size() == new_id);
      lookup.push_back(location);
    } else {
      /* adding in the middle -> reuse old spot */
      lookup.at(new_id) = location;
    }
    assert_2(new_id < lookup.size());

    cached_sum += val.calculated_rate;

    if (location == tree.size()) {
      /* adding at the end */
      tree.emplace_back(val, val.calculated_rate, new_id);
    } else {
      /* adding in the middle, reusing old spot */
      assert_2(location < tree.size());
      tree[location].payload = val;
      tree[location].accumulated += val.calculated_rate;
      tree[location].id = new_id;
    }

    /* restore corret state */
    update_sums(location, val.calculated_rate);

    /* prepare handle object */
    reaction_handle hdl;
    hdl.id = new_id;

    /* verify state */
    assert_2(tree.front().accumulated + 1 > cached_sum);
    assert_2(tree.front().accumulated - 1 < cached_sum);
    assert_3(check_lookup());
    assert_2(lookup.at(hdl.id) < tree.size());
    assert_3(get(hdl) == &tree.at(lookup.at(hdl.id)).payload);

    return hdl;
  }

  /* remove a reaction referd to by hdl */
  void remove(reaction_handle hdl) {

    if (!use_swapping) {
      remove_no_swap(hdl);
      return;
    }

    assert_3(check_lookup());

    const auto idx = lookup.at(hdl.id);

    cached_sum -= tree.at(idx).payload.calculated_rate;

    // if not last element, swap
    if (idx + 1 < tree.size()) {
      assert_3(lookup.at(tree[idx].id) == idx);
      std::swap(tree[idx], tree.back());
      // assert_3(lookup.at(tree[idx].idx) != idx);
      lookup.at(tree[idx].id) = idx;
      tree[idx].accumulated = tree.back().accumulated - tree.back().payload.calculated_rate +
                              tree[idx].payload.calculated_rate;
      update_sums(idx, +tree[idx].payload.calculated_rate - tree.back().payload.calculated_rate);
    }

    // Removing last element
    auto last_idx = tree.size() - 1;
    assert_2(last_idx + 1 == tree.size());
    tree.back().accumulated = 0;
    update_sums(last_idx, -tree[idx].payload.calculated_rate);
    lookup.at(tree.back().id) = 11111111;
    tree.back().payload.remove();
    tree.pop_back();

    available_id.push(hdl.id);

    assert_2(tree.front().accumulated + 1 > cached_sum);
    assert_2(tree.front().accumulated - 1 < cached_sum);

    assert_3(check_lookup());
  }

  /* is the container empty? */
  bool empty() { return size() == 0; }

  /* modification of an element requers updating of the branch in the tree */
  void modify(const reaction_handle &hdl, const double old_rate, const double new_rate) {
    cached_sum -= old_rate;
    cached_sum += new_rate;

    update_sums(lookup.at(hdl.id), new_rate - old_rate);
    tree[lookup[hdl.id]].accumulated += -old_rate + new_rate;

    assert_3(check_lookup());
  }

  /* retrive the undelying reaction of a hdl object */
  T *get(const reaction_handle &hdl) {
    assert_2(hdl.id < lookup.size());
    assert_2(lookup.at(hdl.id) < tree.size());
    return &(tree.at(lookup.at(hdl.id)).payload);
  }

  /* Select the next reaction to fire. Returend are the reaction and the timestep delta T */
  std::pair<T *, double> select() {
    assert_3(check_lookup());

    const double delta_t = get_exponetial(tree.front().accumulated);

    std::uniform_real_distribution<double> zeroOneDist(0, 1);
    double selector = zeroOneDist(mersenneGenerator) * tree.front().accumulated;
    size_t idx = 0;
    assert_2(idx < tree.size());

    while (selector - tree[idx].payload.calculated_rate > 0) {
      assert_2(tree[idx].accumulated >= selector);
      if (test_level >= 3) {
        double sub_sum = 0;
        for (int i = 1; i <= node_size && idx * node_size + i < tree.size(); i++) {
          sub_sum += tree.at(idx * node_size + i).accumulated;
        }

        assert_3(sub_sum + .1 > tree[idx].accumulated - tree[idx].payload.calculated_rate);
        assert_3(sub_sum - .1 < tree[idx].accumulated - tree[idx].payload.calculated_rate);
      }

      selector -= tree[idx].payload.calculated_rate;
      const auto old_idx = idx;

      for (int i = 1; i <= node_size; i++) {

        if (test_level >= 3) {
          double sub_sum = 0;
          for (int k = i; (k <= node_size) && (idx * node_size + k < tree.size()); k++) {
            sub_sum += tree.at(idx * node_size + k).accumulated;
          }
          assert_3(sub_sum + .01 > selector);
        }

        if (idx * node_size + i > tree.size()) {
          throw std::runtime_error("rate selection error");
        }
        if (selector - tree[idx * node_size + i].accumulated <= 0) {
          idx = idx * node_size + i;
          assert_3(idx < tree.size());
          i = node_size + 1; // ending for loop
        } else {
          selector -= tree[idx * node_size + i].accumulated;
          assert_2(i < node_size);
        }
      }
      assert_1(old_idx != idx);
      assert_2(idx < tree.size());
    }
    return std::make_pair(&tree.at(idx).payload, delta_t);
  }

  /* get the sum of all reaction propenseties */
  double get_sum() {
    assert_3(get_sum_slow() * .99 - .001 <= cached_sum);
    assert_3(get_sum_slow() * 1.01 + .001 >= cached_sum);
    return cached_sum;
  }

  /* Used for debuning and verification */
  double get_sum_slow() {
    double sum = 0;
    for (const auto &val : tree) {
      sum += val.payload.calculated_rate;
    }
    return sum;
  }

  /* standart C++ type performance optimization */
  void reserve(size_t s) {
    lookup.reserve(s);
    tree.reserve(s);
  }

  /* how many reactions are stored */
  size_t size() const {
    if (use_swapping) {
      return tree.size();
    } else {
      return tree.size() - available_id.size();
    }
  }

private:
#ifdef FANOUT
  const static int node_size = FANOUT;
#else
  const static int node_size = 16;
#endif

#ifdef USE_SWAPPING
  const static bool use_swapping = true;
#else
  const static bool use_swapping = false;
#endif

  void remove_no_swap(reaction_handle &hdl) {
    const auto location = lookup.at(hdl.id);

    tree.at(location).payload.remove();

    modify(hdl, tree.at(location).payload.calculated_rate, 0);

    available_tree_pos.push(lookup.at(hdl.id));
    lookup.at(tree[location].id) = 22222222;
    tree.at(location).payload.calculated_rate = 0;

    available_id.push(hdl.id);
  }

  void swap(int x, int y) {
    if (x == y) {
      return;
    }
    assert_2(x < tree.size());
    assert_2(y < tree.size());
    assert_2(x >= 0);
    assert_2(y >= 0);
    std::swap(tree[x], tree[y]);
    lookup.at(tree[x].id) = x;
    lookup.at(tree[y].id) = y;
    const auto x_acc = tree[x].accumulated;
    tree[x].accumulated =
        tree[y].accumulated - tree[y].payload.calculated_rate + tree[x].payload.calculated_rate;
    tree[y].accumulated = x_acc - tree[x].payload.calculated_rate + tree[y].payload.calculated_rate;
    update_sums(x, +tree[x].payload.calculated_rate - tree[y].payload.calculated_rate);
    update_sums(y, +tree[y].payload.calculated_rate - tree[x].payload.calculated_rate);
  }

  void update_sums(int i, double val) {
    while (i > 0) {
      const int old_i = i;
      i = std::floor((i - 1.) / node_size);
      tree.at(i).accumulated += val;
    }
  }

  struct node {

    T payload;
    size_t id;
    double accumulated;
    node(const T &val, const double &sum, size_t lookup_id) {
      payload = val;
      accumulated = sum;
      id = lookup_id;
    }
  };
  std::vector<node> tree;
  std::vector<size_t> lookup;
  bool check_lookup() {
    for (int id = 0; id < lookup.size(); id++) {
      if (lookup.at(id) != 11111111 && lookup.at(id) != 22222222) {
        if (id != tree.at(lookup.at(id)).id) {
          std::cout << " Err: " << id << " != " << tree.at(lookup.at(id)).id << " @ "
                    << lookup.at(id) << "\n";
          return false;
        }
      }
    }
    return true;
  }

  double cached_sum = 0;

  bool is_valid(int idx) {
    double sub_sum = 0;
    for (int k = 1; (k <= node_size) && (idx * node_size + k < tree.size()); k++) {
      sub_sum += tree.at(idx * node_size + k).accumulated;
    }
    if (sub_sum + .1 < tree.at(idx).accumulated)
      ;
  }

  std::stack<size_t> available_id;
  size_t next_id = 0;

  size_t get_id() {
    if (!available_id.empty()) {
      const size_t val = available_id.top();
      available_id.pop();
      return val;
    }
    auto val = next_id;
    next_id++;
    return val;
  }

  std::stack<size_t> available_tree_pos;

  size_t get_tree_pos() {
    if (!available_tree_pos.empty()) {
      const size_t val = available_tree_pos.top();
      available_tree_pos.pop();
      assert_2(val < tree.size());
      return val;
    }
    auto val = tree.size();
    return val;
  }

  std::random_device rd;
  std::mt19937_64 mersenneGenerator = std::mt19937_64(rd());

  double get_exponetial(double rate) {
    std::exponential_distribution<double> expoDist(rate);
    return expoDist(mersenneGenerator);
  }
};
