# Readme
This repository contains the header only C++ implementations of containers for dynamic sets of reactions for the *Stochastic Simulation Algorithm*. They are implemented using Templates and thus should be very easy to integrate into an existing code-base.

## Associated Publication
The short-paper *Handling Dynamic Sets of Reactions in Stochastic Simulation Algorithms* will be published at the 2018 Conference Principles of Advanced Discrete Simulation ([ACM SIGSIM PADS](https://www.acm-sigsim-pads.org/)).
The paper contains a description of the algorithm/data structures as well as, a performance evaluation.

## Requirements
The NRM-implementation relies on [boost.heap](http://www.boost.org/doc/libs/release/libs/heap/). Boost is readily available on most platforms. The tree implementation is header-only.

## Usage
You can continue using your own reaction object implementation, and need only specify it as a Template, when instantiating the container:
```
reaction_store<my_reaction> my_container;
```
The `my_reaction` object is only required to be copyable and have a `.calculated_rate` attribute returning the propensity.

Now you can easily add/remove/modify reactions in the container:
```
reaction_store<my_reaction> my_container;
auto hdl = my_container.add(some_reaction);
my_container.remove(hdl)
```
If you want to modify the rate of any reaction, first change it, so that `calculated_rate` returns the new value, then call the `.modify` function.

## Debug
There are various assertions specified in the code, to test for correct behavior. In the debug_defines header, these can be switched on and of.

## Author
This Code has been written by [Till Köster](https://mosi.informatik.uni-rostock.de/team/staff/)
